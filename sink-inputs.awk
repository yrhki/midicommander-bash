BEGIN {
    FS=": | = "
    OFS=","
}

{
    gsub(/\t/, "", $1)
    gsub(/ $/, "", $2)
}

{
    if ($1 ~ "index") {
        INDEX = $2
        sinks[INDEX]["index"] = $2
    }
    else {
        if ($1 ~ /^.+balance/) {
            split($1, a, " ")
            sinks[INDEX][a[1]] = a[2] 
        }
        if ($2 != "") {
            gsub(/"/, "", $2) #"
            sinks[INDEX][$1] = $2
        }
    }
}

END {
    for (s in sinks) {
        printf "%s|%s|%s|%s\n",sinks[s]["index"],sinks[s]["application.name"],sinks[s]["application.process.id"],sinks[s]["application.process.binary"]
        
    }
}