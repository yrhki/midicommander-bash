#!/usr/bin/env bash
# -*- coding: utf-8 -*-
export LC_ALL=en_US.UTF-8

cd "$(dirname $(realpath $0))"
# How much is current value of max
function cc_percentage() {
    value_raw=$(bc <<< "scale=2; $1/127")
    echo "${value_raw/./}"
}

function check_control() {
    # Override
    [ "$CONTROL_ASSIGNED" == 4 ] && return 1

    IFS="-" read control subcontrol <<< "$1"
    #echo "$control | $subcontrol"

    # Control type
    [ "${1:0:1}" != "${INPUT_EVENT_1:0:1}" ] && return 1
    # Control number
    [ "${control:1}" != "$INPUT_DATA_1" ] &&  return 1

    local OUTPUT=0


    case $INPUT_EVENT_1 in
        "Control")
            # Starts with c
            ;;
        "Note")
            # Starts with n

            # Only on "Note on"
            [[ "$2" == "noteon" ]] && [[ "$INPUT_EVENT_2" == "on" ]] || OUTPUT=1

            ;;
        "Program")
            # Starts with p
            echo "HMM"
            ;;
        "System")
            [ "$INPUT_DATA_2" != "$subcontrol" ] && OUTPUT=1
            # System exclusive
            ;;
    esac


    return "$OUTPUT"
}

# Get application id
function get_input_sinks() {
    pacmd list-sink-inputs | awk -f sink-inputs.awk
}

function get_sink_byname() {
    get_input_sinks | grep -E "$1"
}

function get_input_games() {
    sinks=$(get_input_sinks)
    while IFS="|" read index name pid bin; do
        local is_game=false
        local pwd=$(pwdx $pid)

        # Steam games
        [ "$(echo $pwd | grep 'steamapps/common')" ] && is_game=true
        
        [ $is_game = true ] && echo "$index|$name|$pid|$bin"
    done <<< "$(get_input_sinks)"
}

# Get output id
function get_audio_card() {

    IFS=: read index name <<< $(
     pacmd list-cards |
      tr '\n' '\r' |
       perl -pe 's/ *index: ([0-9]+).+?device\.description = "([^\r]+)"\r.+?(?=index:|$)/\1:\2\r/g' |
        tr '\r' '\n' |
         grep "$1")
    echo $index

}

# Print single line output
function good_print() {
    echo -en "\r$1                                      \t"
}

function cc_audio_control() {
    # Check validity
    check_control "$2" noteon || return

    local CONTROL_TARGET_NAME=$4
    local CONTROL_TARGET_TYPE=$1
    local CONTROL_TYPE=$3
    local CONTROL_INDEX=""

    if [ "$CONTROL_TARGET_TYPE" == "device" ]; then
        # Set output
        CONTROL_INDEX=$(get_audio_card $CONTROL_TARGET_NAME)
        CONTROL_COMMAND="set-sink-volume"
    elif [ "$CONTROL_TARGET_TYPE" == "application" ]; then
        # Set application
        IFS="|" read CONTROL_INDEX CONTROL_TARGET_NAME T_PID T_BIN <<< "$(get_sink_byname $CONTROL_TARGET_NAME)"
        #get_sink_byname $3 | read CONTROL_INDEX CONTROL_TARGET_NAME 
        CONTROL_COMMAND="set-sink-input-volume"
    elif [ "$CONTROL_TARGET_TYPE" == "game" ]; then
        IFS="|" read CONTROL_INDEX CONTROL_TARGET_NAME T_PID T_BIN  <<< "$(get_input_games)"
        CONTROL_COMMAND="set-sink-input-volume"

    fi

    # Target was not found
    [ "$CONTROL_INDEX" == "" ] && CONTROL_ASSIGNED=2 && return

    # Check if mute or volume control
    [ "$CONTROL_TYPE" == "mute" ] && CONTROL_COMMAND=$(echo $CONTROL_COMMAND | sed "s/volume/mute/g")
    CONTROL_VALUE=$([ "$CONTROL_TYPE" == mute ] && echo toggle || echo "$(cc_percentage $INPUT_DATA_2)%")

    # Ececute volume change
    pactl $CONTROL_COMMAND $CONTROL_INDEX $CONTROL_VALUE

    # Output change
    good_print "$CONTROL_TARGET_NAME: $CONTROL_VALUE"
    CONTROL_ASSIGNED=1
}

function plasma_browser_integration() {
    # Check validity
    [ "$INPUT_DATA_2" == "" ] && return             # No control data

    check_control "$1" noteon || return 
    local CONTROL_VALUE=$(bc <<< "scale=2; $INPUT_DATA_2/127")
    local OPTIONS=()
    
    local MOVE_LENGTH=10000000

    case "$2" in
        "volume")       OPTIONS+=(Volume "$CONTROL_VALUE");;
        "raise")        OPTIONS+=(Raise);;
        "playpause")    OPTIONS+=(PlayPause);;
        "next")         OPTIONS+=(Next);;
        "previous")     OPTIONS+=(Previous);;
        "forward")      OPTIONS+=(SetPosition / "$(bc <<< "$(qdbus org.kde.plasma.browser_integration /org/mpris/MediaPlayer2 Position)+$MOVE_LENGTH")");;
        "backward")     OPTIONS+=(SetPosition / "$(bc <<< "$(qdbus org.kde.plasma.browser_integration /org/mpris/MediaPlayer2 Position)-$MOVE_LENGTH")");;
    esac

    qdbus org.kde.plasma.browser_integration /org/mpris/MediaPlayer2 "${OPTIONS[@]}"


    local PRINT=${OPTIONS[@]}
    good_print "Plasma Browser Integration: $PRINT"
    CONTROL_ASSIGNED=4
}

aseqdump -p 'X-TOUCH MINI' | \
while IFS=" ," read src INPUT_EVENT_1 INPUT_EVENT_2 INPUT_CHANNEL label1 INPUT_DATA_1 label2 INPUT_DATA_2 rest; do
    [ "$INPUT_DATA_1" == "to" ] && continue # Skip header
    active_window="$(cat /proc/$(xdotool getactivewindow getwindowpid)/comm)"
    [ "$CONTROL_ASSIGNED" == "5" ] && CONTROL_ASSIGNED=0 && continue

    CONTROL_ASSIGNED=0

    # deal with program change CC0 and CC32
    check_control C0 && CONTROL_ASSIGNED=5 && continue

    # Command        target type    Control target name     control type
    cc_audio_control application    C11     volume          foobar2000          
    cc_audio_control application    C31     mute            foobar2000
    cc_audio_control device         C1      volume          'Built-in Audio'    
    cc_audio_control game           C17     volume
    cc_audio_control game           N101    mute
    cc_audio_control application    C12     volume          freetube
    cc_audio_control application    C32     mute            freetube

    plasma_browser_integration C38 raise
    plasma_browser_integration C18 volume
    plasma_browser_integration N39 playpause   

    case "$(ps -p $(xdotool getactivewindow getwindowpid) -o comm=)" in
        "firefox")

    #        check_control

            ;;
    esac

    # If target wasn't available or control not assigned
    [ $CONTROL_ASSIGNED == 0 ] && good_print "UNASSIGNED: $INPUT_EVENT_1 $INPUT_EVENT_2 $INPUT_DATA_1 $INPUT_DATA_2" 
    [ $CONTROL_ASSIGNED == 2 ] && good_print "NOT FOUND: $INPUT_EVENT_1 $INPUT_EVENT_2 $INPUT_DATA_1 $INPUT_DATA_2"


done
